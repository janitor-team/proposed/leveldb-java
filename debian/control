Source: leveldb-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends:
 debhelper (>= 11),
 default-jdk,
 libguava-java,
 libsnappy-java,
 maven-debian-helper (>= 2.0)
Standards-Version: 4.2.1
Vcs-Git: https://salsa.debian.org/java-team/leveldb-java.git
Vcs-Browser: https://salsa.debian.org/java-team/leveldb-java
Homepage: http://github.com/dain/leveldb

Package: libleveldb-api-java
Architecture: all
Depends: ${misc:Depends}
Suggests: libleveldb-java
Description: High level Java API for LevelDB
 Port of LevelDB in Java with the goal of having a feature complete
 implementation that is within 10% of the performance of the C++ original
 and produces byte-for-byte exact copies of the C++ code.
 .
 This package contains the high level API for LevelDB.

Package: libleveldb-java
Architecture: all
Depends: ${misc:Depends}, libleveldb-api-java, libguava-java
Suggests: libsnappy-java
Description: Port of LevelDB to Java
 Port of LevelDB in Java with the goal of having a feature complete
 implementation that is within 10% of the performance of the C++ original
 and produces byte-for-byte exact copies of the C++ code.
 .
 This package contains the implementation of the LevelDB API.
